CFLAGS+=-O2
CFLAGS+=-Wall
CFLAGS+=-Werror
CFLAGS+=-D_GNU_SOURCE

LDFLAGS+=-lcwiid
LDFLAGS+=-lbluetooth

all: wiipdf

wiipdf: wiipdf.c
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

clean:
	rm -f wiipdf

distclean: clean

install: wiipdf
	install -d -m 755 $(DESTDIR)/usr/bin
	install -d -m 755 $(DESTDIR)/usr/share/man/man1
	install -m 755 wiipdf $(DESTDIR)/usr/bin/wiipdf
	install -m 644 wiipdf.1 $(DESTDIR)/usr/share/man/man1/wiipdf.1
